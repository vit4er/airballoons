USE [master]
GO
/****** Object:  Database [airballsdb]    Script Date: 2019-11-07 8:43:57 PM ******/
CREATE DATABASE [airballsdb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'airballsdb', FILENAME = N'd:\StudioProjects\Airballs\Airballs.API\App_Data\airballsdb.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'airballsdb_log', FILENAME = N'd:\StudioProjects\Airballs\Airballs.API\App_Data\airballsdb_Log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [airballsdb] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [airballsdb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [airballsdb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [airballsdb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [airballsdb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [airballsdb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [airballsdb] SET ARITHABORT OFF 
GO
ALTER DATABASE [airballsdb] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [airballsdb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [airballsdb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [airballsdb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [airballsdb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [airballsdb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [airballsdb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [airballsdb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [airballsdb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [airballsdb] SET  DISABLE_BROKER 
GO
ALTER DATABASE [airballsdb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [airballsdb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [airballsdb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [airballsdb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [airballsdb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [airballsdb] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [airballsdb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [airballsdb] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [airballsdb] SET  MULTI_USER 
GO
ALTER DATABASE [airballsdb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [airballsdb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [airballsdb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [airballsdb] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [airballsdb] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [airballsdb] SET QUERY_STORE = OFF
GO
USE [airballsdb]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [airballsdb]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 2019-11-07 8:43:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Airballs]    Script Date: 2019-11-07 8:43:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Airballs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[ColorCode] [nvarchar](450) NULL,
	[Description] [nvarchar](max) NULL,
	[Price] [decimal](18, 2) NOT NULL,
	[Quantity] [int] NOT NULL,
 CONSTRAINT [PK_Airballs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Colors]    Script Date: 2019-11-07 8:43:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Colors](
	[Code] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_Colors] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20191023143105_Init', N'2.2.6-servicing-10079')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20191023150153_AirballColor', N'2.2.6-servicing-10079')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20191023231817_ColorCodeNeverGenerate', N'2.2.6-servicing-10079')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20191024192320_AirballoonsChanges', N'2.2.6-servicing-10079')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20191026063824_UpdatingAirball', N'2.2.6-servicing-10079')
SET IDENTITY_INSERT [dbo].[Airballs] ON 

INSERT [dbo].[Airballs] ([Id], [Name], [ColorCode], [Description], [Price], [Quantity]) VALUES (5, N'Шар обыкновенный', N'#0000FF', N'Круглый, без надписи.', CAST(15.90 AS Decimal(18, 2)), 76)
INSERT [dbo].[Airballs] ([Id], [Name], [ColorCode], [Description], [Price], [Quantity]) VALUES (6, N'Шар обыкновенный', N'#00FF00', N'Круглый, без надписи', CAST(15.90 AS Decimal(18, 2)), 20)
INSERT [dbo].[Airballs] ([Id], [Name], [ColorCode], [Description], [Price], [Quantity]) VALUES (7, N'Шар обыкновенный', N'#FF0000', N'Круглый, без надписи', CAST(15.90 AS Decimal(18, 2)), 60)
INSERT [dbo].[Airballs] ([Id], [Name], [ColorCode], [Description], [Price], [Quantity]) VALUES (8, N'Шар обыкновенный', N'#FFFF00', N'Круглый, без надписи', CAST(15.90 AS Decimal(18, 2)), 44)
INSERT [dbo].[Airballs] ([Id], [Name], [ColorCode], [Description], [Price], [Quantity]) VALUES (9, N'Шар "Сердце"', N'#FF0000', N'На день Св. Валентина', CAST(49.90 AS Decimal(18, 2)), 99)
INSERT [dbo].[Airballs] ([Id], [Name], [ColorCode], [Description], [Price], [Quantity]) VALUES (10, N'Шар "Сердце"', N'#FF0000', N'Шар в форме сердца, с надписью LOVE', CAST(49.90 AS Decimal(18, 2)), 60)
INSERT [dbo].[Airballs] ([Id], [Name], [ColorCode], [Description], [Price], [Quantity]) VALUES (11, N'Шар "Лошарик"', N'#00FF00', N'Шар в форме Лошарика', CAST(49.90 AS Decimal(18, 2)), 20)
INSERT [dbo].[Airballs] ([Id], [Name], [ColorCode], [Description], [Price], [Quantity]) VALUES (12, N'Шар "Лошарик"', N'#FFFF00', N'Шар в форме Лошарика', CAST(49.90 AS Decimal(18, 2)), 20)
INSERT [dbo].[Airballs] ([Id], [Name], [ColorCode], [Description], [Price], [Quantity]) VALUES (13, N'Шар - смайлик', N'#FFFF00', N'Изображение смайлика', CAST(33.50 AS Decimal(18, 2)), 15)
INSERT [dbo].[Airballs] ([Id], [Name], [ColorCode], [Description], [Price], [Quantity]) VALUES (14, N'Шар Happy Birthday', N'#0000FF', N'Шар с надписью Happy Birthday', CAST(20.00 AS Decimal(18, 2)), 10)
SET IDENTITY_INSERT [dbo].[Airballs] OFF
INSERT [dbo].[Colors] ([Code], [Name]) VALUES (N'#0000FF', N'Синий')
INSERT [dbo].[Colors] ([Code], [Name]) VALUES (N'#00FF00', N'Зеленый')
INSERT [dbo].[Colors] ([Code], [Name]) VALUES (N'#FF0000', N'Красный')
INSERT [dbo].[Colors] ([Code], [Name]) VALUES (N'#FFFF00', N'Желтый')
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_Airballs_ColorCode]    Script Date: 2019-11-07 8:43:58 PM ******/
CREATE NONCLUSTERED INDEX [IX_Airballs_ColorCode] ON [dbo].[Airballs]
(
	[ColorCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Airballs] ADD  DEFAULT ((0.0)) FOR [Price]
GO
ALTER TABLE [dbo].[Airballs] ADD  DEFAULT ((0)) FOR [Quantity]
GO
ALTER TABLE [dbo].[Airballs]  WITH CHECK ADD  CONSTRAINT [FK_Airballs_Colors_ColorCode] FOREIGN KEY([ColorCode])
REFERENCES [dbo].[Colors] ([Code])
GO
ALTER TABLE [dbo].[Airballs] CHECK CONSTRAINT [FK_Airballs_Colors_ColorCode]
GO
USE [master]
GO
ALTER DATABASE [airballsdb] SET  READ_WRITE 
GO
